package com.tsystems.javaschool.tasks.pyramid;

import java.rmi.UnexpectedException;
import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        Scanner sc = new Scanner(System.in);
        int size = inputNumbers.size();
        int highOfPyramid = (int) ((1 + Math.sqrt(1 + 8 * size)) / 2);
        if (highOfPyramid - (1 + Math.sqrt(1 + 8 * size)) / 2 != 0) {
            throw new CannotBuildPyramidException();
        }
        System.out.println(highOfPyramid - 1);
        highOfPyramid -= 1;
        try {
            inputNumbers.sort(Comparator.naturalOrder());
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        int baseIterator = 0;
        int[][] tableTree = new int[highOfPyramid][2 * highOfPyramid - 1];
        int tableTreeHighIterator = 0;
        int tableTreeLengthIterator = 0;
        for (int level = 1; level <= highOfPyramid; level++) {
            int j = 0;
            while (j < highOfPyramid - level) {
                tableTree[tableTreeHighIterator][tableTreeLengthIterator] = 0;
                tableTreeLengthIterator++;
                j++;
            }
            for (int k = 0; k < level; k++) {
                tableTree[tableTreeHighIterator][tableTreeLengthIterator] = inputNumbers.get(baseIterator);
                baseIterator++;
                tableTreeLengthIterator++;
                if (k != level - 1) {
                    tableTree[tableTreeHighIterator][tableTreeLengthIterator] = 0;
                    tableTreeLengthIterator++;
                }
            }
            while (j > 0) {
                tableTree[tableTreeHighIterator][tableTreeLengthIterator] = 0;
                tableTreeLengthIterator++;
                j--;
            }
            tableTreeHighIterator++;
            tableTreeLengthIterator = 0;
        }
        return tableTree;
    }


}