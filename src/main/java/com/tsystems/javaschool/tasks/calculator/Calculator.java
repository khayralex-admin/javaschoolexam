package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
    public enum ArithmeticalExpressionType {
        PlUS,
        MINUS,
        LEFT,
        RIGHT,
        MULTIPLE,
        DIVIDE,
        CONST,
        EOF
    }

    public class StringPart {
        ArithmeticalExpressionType type;
        String value;

        public StringPart(ArithmeticalExpressionType type, String value) {
            this.type = type;
            this.value = value;
        }
    }

    public class StringPartBuffer {
        private int pos = 0;

        public List<StringPart> base;

        public StringPartBuffer(List<StringPart> list) {
            this.base = list;
        }

        public StringPart next() {
            return base.get(pos++);
        }

        public void back() {
            pos--;
        }
    }

    public class ExpressionParser {
        private int pos = 0;
        private String expression;
        public boolean isNull = false;


        private void skipWhitespaces() {
            while (pos < expression.length() && Character.isWhitespace(expression.charAt(pos))) {
                pos++;
            }
        }

        public double parse(String s) {
            pos = 0;
            expression = s;
            return parseMaker(new StringPartBuffer(this.makeSPList()));
        }

        private List<StringPart> makeSPList() {
            int leftCnt = 0;
            int rightCnt = 0;
            List<StringPart> base = new ArrayList<>();
            while (pos < expression.length() && leftCnt >= rightCnt) {
                skipWhitespaces();
                if (pos >= expression.length()) {
                    break;
                }
                char cur = expression.charAt(pos);
                if (cur == '(') {
                    leftCnt++;
                    pos++;
                    base.add(new StringPart(ArithmeticalExpressionType.LEFT, "("));
                } else if (cur == '+') {
                    pos++;
                    base.add(new StringPart(ArithmeticalExpressionType.PlUS, "+"));
                } else if (cur == '*') {
                    pos++;
                    base.add(new StringPart(ArithmeticalExpressionType.MULTIPLE, "*"));
                } else if (cur == '-') {
                    pos++;
                    base.add(new StringPart(ArithmeticalExpressionType.MINUS, "-"));
                } else if (cur == '/') {
                    pos++;
                    base.add(new StringPart(ArithmeticalExpressionType.DIVIDE, "/"));
                } else if (cur == ')') {
                    rightCnt++;
                    pos++;
                    base.add(new StringPart(ArithmeticalExpressionType.RIGHT, ")"));
                } else if (cur <= '9' && cur >= '0') {
                    StringBuilder number = new StringBuilder();
                    while (pos < expression.length() && ((expression.charAt(pos) <= '9' && expression.charAt(pos) >= '0') || (expression.charAt(pos) == '.'))) {
                        number.append(expression.charAt(pos));
                        pos++;
                    }
                    base.add(new StringPart(ArithmeticalExpressionType.CONST, number.toString()));
                } else {
                    isNull = true;
                    break;
                }
            }
            if (leftCnt != rightCnt) {
                isNull = true;
            }
            base.add(new StringPart(ArithmeticalExpressionType.EOF, ""));
            return base;
        }

        private double parseMaker(StringPartBuffer parts) {
            StringPart part = parts.next();
            if (part.type == ArithmeticalExpressionType.EOF) {
                isNull = true;
                return 1;
            } else {
                parts.back();
                return thirdPriority(parts);
            }
        }

        private double thirdPriority(StringPartBuffer parts) {
            double value = secondPriority(parts);
            while (!isNull) {
                StringPart part = parts.next();
                if (part.type == ArithmeticalExpressionType.EOF || part.type == ArithmeticalExpressionType.RIGHT) {
                    parts.back();
                    return value;
                } else {
                    isNull = true;
                }
            }
            return 1;
        }

        private double secondPriority(StringPartBuffer parts) {
            double value = firstPriority(parts);
            while (!isNull) {
                StringPart part = parts.next();
                if (part.type == ArithmeticalExpressionType.PlUS) {
                    value = value + firstPriority(parts);
                } else if (part.type == ArithmeticalExpressionType.MINUS) {
                    value = value - firstPriority(parts);
                } else if (part.type == ArithmeticalExpressionType.EOF || part.type == ArithmeticalExpressionType.RIGHT) {
                    parts.back();
                    return value;
                } else {
                    isNull = true;
                }
            }
            return 1;
        }

        public boolean returnNull() {
            return isNull;
        }

        private double firstPriority(StringPartBuffer parts) {
            double value = zeroPriority(parts);
            while (!isNull) {
                StringPart part = parts.next();
                if (part.type == ArithmeticalExpressionType.MULTIPLE) {
                    value = value * zeroPriority(parts);
                } else if (part.type == ArithmeticalExpressionType.DIVIDE) {
                    double denominator = zeroPriority(parts);
                    if (denominator != 0)
                        value = value / denominator;
                    else isNull = true;
                } else if (part.type == ArithmeticalExpressionType.EOF || part.type == ArithmeticalExpressionType.RIGHT ||
                        part.type == ArithmeticalExpressionType.PlUS || part.type == ArithmeticalExpressionType.MINUS) {
                    parts.back();
                    return value;
                } else {
                    isNull = true;
                }
            }
            return 1;
        }

        private double zeroPriority(StringPartBuffer parts) {
            if (!isNull) {
                StringPart part = parts.next();
                switch (part.type) {
                    case CONST:
                        try {
                            return Double.parseDouble(part.value);
                        } catch (NumberFormatException e) {
                            isNull = true;
                            return 1;
                        }
                    case LEFT:
                        double value = thirdPriority(parts);
                        part = parts.next();
                        if (part.type != ArithmeticalExpressionType.RIGHT) {
                            isNull = true;
                        }
                        return value;
                    default: {
                        isNull = true;
                    }
                }
            }
            return 1;
        }

    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == (null)) {
            return null;
        }
        ExpressionParser parser = new ExpressionParser();
        double ans = parser.parse(statement);
        if (parser.returnNull()) {
            return null;
        }
        if (ans % 1 == 0) {
            return String.valueOf((int) ans);
        } else return String.valueOf(ans);
    }

}
