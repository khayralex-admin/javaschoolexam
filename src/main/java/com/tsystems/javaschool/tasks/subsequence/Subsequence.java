package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null) {
            throw new IllegalArgumentException();
        }
        if (y == null) {
            throw new IllegalArgumentException();
        }
        int iteratorX = 0;
        int iteratorY = 0;
        int counter = 0;
        while (iteratorY != y.size() && iteratorX != x.size()) {
            if (y.get(iteratorY).equals(x.get(iteratorX))) {
                counter++;
                iteratorX++;
                iteratorY++;
            } else iteratorY++;
        }
        return counter == x.size();
    }
}
